using System;
using org.apache.cayenne;
using org.apache.cayenne.configuration.server;
using org.apache.log4j;
using com.amphora.cayenne.config;
using com.amphora.cayenne.dao;
using org.example.cayenne.persistent;

namespace ikvmpoc
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Logger.getRootLogger().setLevel(Level.OFF);
			ServerRuntime runtime = new ServerRuntime("cayenne-project.xml");
			
			ObjectContext obj = runtime.newContext();
			//Console.WriteLine ("Hello World!" + obj.ToString());
			
			//AmphoraConfiguration config = AmphoraConfigurationHelper.getConfiguration("base-applicationContext");
		 	
			Artist art = new Artist();
			art.setName("Biana Andrea");
			//art.setBdate(new java.util.Date());
			
			
			//com.amphora.cayenne.dao.ArtistDAO daoA =  (com.amphora.cayenne.dao.ArtistDAO)config.getSpringBean("com.amphora.cayenne.dao.ArtistDAO");
			//daoA.store(art);
			//obj.registerNewObject(art);
			com.amphora.cayenne.dao.ArtistDAO artDao = new com.amphora.cayenne.dao.impl.ArtistDAOImpl();
			((com.amphora.cayenne.dao.impl.SpringCayenneDAOImpl)artDao).setCayenneObjectContext(obj);
			
			artDao.store(art);
			
			Artist a1 = (org.example.cayenne.persistent.Artist) artDao.findById(art.getId());
			Console.WriteLine(" Artist Inserted." + a1.toString());
		}
	}
}
